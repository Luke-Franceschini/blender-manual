.. index:: Geometry Nodes; Blend Hair Curves

*****************
Blend Hair Curves
*****************

Blends shape between multiple hair curves in a certain radius.

.. peertube:: kegHEYG8URADfPpBZ4jRUo

Inputs
======

Geometry
   Input Geometry (only curves will be affected).

Factor
   Factor to blend overall effect.

Blend Radius
   Radius to select neighbors for blending.

Blend Neighbors
   Amount of neighbors used for blending.

Preserve Length
   Preserve each curve's length during deformation.


Properties
==========

This node has no properties.


Outputs
=======

**Geometry**
